// Теоретичні питання
// 1. Опишіть своїми словами, що таке метод об'єкту
//  Обєкт - це колекція па (ключ - значення), коли значення є функцією, то це стає методом, який визначає поведінку обєкта

// 2. Який тип даних може мати значення властивості об'єкта?
//  Будь=яке значенняю На прикладЖ число, рядок, масив, функція

// 3. Об'єкт це посилальний тип даних. Що означає це поняття?
// Посилальний обєкт займає лише одну комірку в стеку, а вся решта інформаціа про обєкт знаходиться в хіпі. 
// В тій комірці стеку є посилання за яким знаходться всі решта дані.






// Практичні завдання
// 1. Створіть об'єкт product з властивостями name, price та discount. 
//Додайте метод для виведення повної ціни товару з урахуванням знижки. 
//Викличте цей метод та результат виведіть в консоль.
let product = {
    name: 'Parfume',
    price: 203001,
    discount: 0.9,
}

function yourPrice(item) {
   let discountedPrice = item.price * (1 - item.discount);
    return discountedPrice;
}

console.log(yourPrice(product));




// 2. Напишіть функцію greeting, яка приймає об'єкт з властивостями name та age, і повертає рядок з привітанням і віком,

// наприклад "Привіт, мені 30 років".

// Попросіть користувача ввести своє ім'я та вік за допомогою prompt, 
//і викличте функцію gteeting з введеними даними(передавши їх як аргументи). 
//Результат виклику функції виведіть з допомогою alert.

function greeting(person) {
    return `Hello, my name is ${person.name} (but who cares 🫥 ). I'm ${person.age}).`;
}

let homosapiens = {
    name: prompt("Please enter your name!"),
    age: Number(prompt("Please enter your age!")),
}
console.log(greeting(homosapiens));




// 3.Опціональне. Завдання:

// Реалізувати повне клонування об'єкта.

let good = {
    type: 'shoes',
    model: 'Godess',
    color: 'golden',
    sizes: {
        heel: 12,
        width: 6,
        length: 25,
    }, 
    sizesInStock: [5, 5.5, 6, 6.5, 7],
}

function clone (source){
    let dest ={};
    
    for (const key in source) {
            const value = source[key];
            let type = typeof(value);
            console.log(type);

            if (type === 'object') {
                console.log('Is an object');
                dest[key] = clone(value);

            } else {
                console.log('Is not an object');
            dest[key] = value;
            }
    }
    return dest;
}
let good2 = clone(good);

good.color = 'silver';
good.sizes.heel = 8;
good.sizesInStock[0] = 4;

console.log(good);
console.log(good2);


// let clone = structuredClone(good);

// good.sizes.heel = 8;

// clone.sizes.heel = 10;

// console.log(good);
// console.log(clone);


// Технічні вимоги:
// - Написати функцію для рекурсивного повного клонування об'єкта (без єдиної передачі за посиланням, 
//внутрішня вкладеність властивостей об'єкта може бути досить великою).

// - Функція має успішно копіювати властивості у вигляді об'єктів та масивів на будь-якому рівні вкладеності.

// - У коді не можна використовувати вбудовані механізми клонування, такі як функція Object.assign() або spread.